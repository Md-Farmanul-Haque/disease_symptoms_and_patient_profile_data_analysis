import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.metrics import classification_report, accuracy_score, precision_score
import matplotlib.pyplot as plt
import os

def load_data(data_path: str) -> pd.DataFrame:
    """
    Load data from CSV file into a pandas DataFrame.
    
    Args:
    - data_path (str): Path to the CSV file.
    
    Returns:
    - pd.DataFrame: Loaded DataFrame containing the data.
    """
    data = pd.read_csv(data_path)
    return data

def preprocess_data(df: pd.DataFrame) -> pd.DataFrame:
    """
    Perform data preprocessing tasks on the DataFrame.
    
    Args:
    - df (pd.DataFrame): Input DataFrame containing health data.
    
    Returns:
    - pd.DataFrame: Preprocessed DataFrame.
    """
    # Convert categorical columns to numeric using LabelEncoder
    le = LabelEncoder()
    categorical_cols = df.select_dtypes(include='object').columns
    for col in categorical_cols:
        df[col] = le.fit_transform(df[col])
    
    return df

def train_models(X_train, y_train):
    """
    Train three different models: Logistic Regression, Random Forest, and SVM.
    
    Args:
    - X_train (pd.DataFrame): Features for training.
    - y_train (pd.Series): Target variable for training.
    
    Returns:
    - dict: Trained model objects.
    """
    models = {}
    
    # Logistic Regression
    lr_model = LogisticRegression(max_iter=1000)
    lr_model.fit(X_train, y_train)
    models['Logistic Regression'] = lr_model
    
    # Random Forest
    rf_model = RandomForestClassifier(random_state=42)
    rf_model.fit(X_train, y_train)
    models['Random Forest'] = rf_model
    
    # SVM
    svm_model = SVC(kernel='linear')
    svm_model.fit(X_train, y_train)
    models['SVM'] = svm_model
    
    return models

def evaluate_models(models: dict, X_test, y_test) -> dict:
    """
    Evaluate trained models using accuracy and precision scores.
    
    Args:
    - models (dict): Dictionary of trained model objects.
    - X_test (pd.DataFrame): Features for testing.
    - y_test (pd.Series): Target variable for testing.
    
    Returns:
    - dict: Evaluation results (accuracy and precision scores) for each model.
    """
    results = {}
    
    for name, model in models.items():
        y_pred = model.predict(X_test)
        report = classification_report(y_test, y_pred, target_names=['Negative', 'Positive'], output_dict=True)
        
        results[name] = {
            'Accuracy': accuracy_score(y_test, y_pred),
            'Precision': precision_score(y_test, y_pred),
            'Recall': report['weighted avg']['recall'],
            'F1-score': report['weighted avg']['f1-score'],
            'Support': report['weighted avg']['support']
        }
    
    return results

def plot_evaluation_metrics(results: dict, save_path: str) -> None:
    """
    Plot and save evaluation metrics (accuracy and precision) as bar charts.
    
    Args:
    - results (dict): Dictionary containing evaluation results for each model.
    - save_path (str): Path to save the generated PNG file.
    
    Returns:
    - None
    """
    plt.figure(figsize=(12, 8))
    
    # Extract model names and scores
    model_names = list(results.keys())
    accuracy_scores = [results[model]['Accuracy'] for model in model_names]
    precision_scores = [results[model]['Precision'] for model in model_names]
    
    # Plot accuracy scores
    ax1 = plt.subplot(2, 1, 1)
    bars1 = ax1.bar(model_names, accuracy_scores, color='skyblue')
    ax1.set_title('Model Accuracy Scores')
    ax1.set_ylabel('Accuracy')
    ax1.set_ylim(0, 1)
    ax1.grid(True)
    plt.setp(ax1.get_xticklabels(), rotation=15, ha='right')  # Rotate x-axis labels for better readability
    
    # Annotate accuracy scores
    for bar, score in zip(bars1, accuracy_scores):
        plt.text(bar.get_x() + bar.get_width() / 2, bar.get_height() - 0.1, round(score, 2),
                 ha='center', color='black', fontsize=10, fontweight='bold')
    
    # Plot precision scores
    ax2 = plt.subplot(2, 1, 2)
    bars2 = ax2.bar(model_names, precision_scores, color='lightgreen')
    ax2.set_title('Model Precision Scores')
    ax2.set_ylabel('Precision')
    ax2.set_ylim(0, 1)
    ax2.grid(True)
    plt.setp(ax2.get_xticklabels(), rotation=15, ha='right')  # Rotate x-axis labels for better readability
    
    # Annotate precision scores
    for bar, score in zip(bars2, precision_scores):
        plt.text(bar.get_x() + bar.get_width() / 2, bar.get_height() - 0.1, round(score, 2),
                 ha='center', color='black', fontsize=10, fontweight='bold')
    
    plt.tight_layout()
    plt.savefig(save_path)
    plt.close()

def plot_detailed_report(results: dict, save_path: str) -> None:
    """
    Plot and save a detailed report of evaluation metrics (accuracy, precision, recall, f1-score, support) for all models.
    
    Args:
    - results (dict): Dictionary containing evaluation results for each model.
    - save_path (str): Path to save the generated PNG file.
    
    Returns:
    - None
    """
    plt.figure(figsize=(16, 10))
    
    metrics = ['Accuracy', 'Precision', 'Recall', 'F1-score', 'Support']
    model_names = list(results.keys())
    
    for i, metric in enumerate(metrics):
        ax = plt.subplot(2, 3, i+1)
        metric_scores = [results[model][metric] for model in model_names]
        bars = ax.bar(model_names, metric_scores, color='lightcoral')
        ax.set_title(f'{metric} Scores')
        ax.set_ylabel(metric)
        ax.set_ylim(0, 1) if metric != 'Support' else None
        ax.grid(True)
        plt.setp(ax.get_xticklabels(), rotation=15, ha='right')  # Rotate x-axis labels for better readability
        
        # Annotate metric scores
        for bar, score in zip(bars, metric_scores):
            plt.text(bar.get_x() + bar.get_width() / 2, bar.get_height() - 0.1, round(score, 2),
                     ha='center', color='black', fontsize=10, fontweight='bold')
    
    plt.tight_layout()
    plt.savefig(save_path)
    plt.close()

if __name__ == "__main__":
    # File path for the dataset
    data_path = '../data/raw/health_data.csv'
    
    # Output directory for saving model evaluation plots
    model_report_dir = '../assets/model_report'
    if not os.path.exists(model_report_dir):
        os.makedirs(model_report_dir)
    
    # Load the dataset
    data = load_data(data_path)
    
    # Preprocess the data
    data = preprocess_data(data)
    
    # Split data into features and target variable
    X = data.drop(columns=['Outcome Variable'])
    y = data['Outcome Variable']
    
    # Split data into training and testing sets
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
    
    # Train models
    trained_models = train_models(X_train, y_train)
    
    # Evaluate models
    evaluation_results = evaluate_models(trained_models, X_test, y_test)
    
    # Plot and save evaluation metrics
    plot_evaluation_metrics(evaluation_results, os.path.join(model_report_dir, 'model_evaluation_metrics.png'))
    
    # Plot and save detailed report
    plot_detailed_report(evaluation_results, os.path.join(model_report_dir, 'detailed_model_evaluation_report.png'))
