import pandas as pd
from sklearn.model_selection import train_test_split, GridSearchCV, cross_val_score, StratifiedKFold
from sklearn.preprocessing import LabelEncoder
from sklearn.ensemble import RandomForestClassifier
from xgboost import XGBClassifier
from sklearn.metrics import classification_report, accuracy_score, precision_score
import matplotlib.pyplot as plt
import os

def load_data(data_path: str) -> pd.DataFrame:
    """
    Load data from CSV file into a pandas DataFrame.
    
    Args:
    - data_path (str): Path to the CSV file.
    
    Returns:
    - pd.DataFrame: Loaded DataFrame containing the data.
    """
    data = pd.read_csv(data_path)
    return data

def preprocess_data(df: pd.DataFrame) -> pd.DataFrame:
    """
    Perform data preprocessing tasks on the DataFrame.
    
    Args:
    - df (pd.DataFrame): Input DataFrame containing health data.
    
    Returns:
    - pd.DataFrame: Preprocessed DataFrame.
    """
    # Convert categorical columns to numeric using LabelEncoder
    le = LabelEncoder()
    categorical_cols = df.select_dtypes(include='object').columns
    for col in categorical_cols:
        df[col] = le.fit_transform(df[col])
    
    return df

def train_optimize_rf_with_xgb(X_train, y_train):
    """
    Train and optimize Random Forest model using XGBoost and GridSearchCV.
    
    Args:
    - X_train (pd.DataFrame): Features for training.
    - y_train (pd.Series): Target variable for training.
    
    Returns:
    - RandomForestClassifier: Optimized Random Forest model.
    """
    # Define base Random Forest classifier
    rf_base = RandomForestClassifier(random_state=42)
    
    # Parameters for grid search
    param_grid = {
        'n_estimators': [50, 100, 150, 200],
        'max_depth': [10, 20, 30, None],
        'min_samples_split': [2, 5, 10],
        'min_samples_leaf': [1, 2, 4],
        'bootstrap': [True, False]
    }
    
    # Initialize GridSearchCV with 10-fold cross-validation
    cv = StratifiedKFold(n_splits=5, shuffle=True, random_state=42)
    grid_search = GridSearchCV(estimator=rf_base, param_grid=param_grid, 
                               cv=cv, scoring='accuracy', verbose=1, n_jobs=-1)
    
    # Fit GridSearchCV to find best parameters
    grid_search.fit(X_train, y_train)
    
    # Get best estimator from GridSearchCV
    best_rf = grid_search.best_estimator_
    
    return best_rf

def evaluate_model(model, X_test, y_test) -> dict:
    """
    Evaluate trained model using accuracy and precision scores.
    
    Args:
    - model: Trained model object.
    - X_test (pd.DataFrame): Features for testing.
    - y_test (pd.Series): Target variable for testing.
    
    Returns:
    - dict: Evaluation results (accuracy and precision scores) for the model.
    """
    y_pred = model.predict(X_test)
    report = classification_report(y_test, y_pred, target_names=['Negative', 'Positive'], output_dict=True)
    
    evaluation_results = {
        'Accuracy': accuracy_score(y_test, y_pred),
        'Precision': precision_score(y_test, y_pred),
        'Recall': report['weighted avg']['recall'],
        'F1-score': report['weighted avg']['f1-score'],
        'Support': report['weighted avg']['support']
    }
    
    return evaluation_results

def plot_evaluation_metrics(results: dict, save_path: str) -> None:
    """
    Plot and save evaluation metrics (accuracy and precision) as bar charts.
    
    Args:
    - results (dict): Dictionary containing evaluation results for the model.
    - save_path (str): Path to save the generated PNG file.
    
    Returns:
    - None
    """
    plt.figure(figsize=(8, 6))
    
    metrics = ['Accuracy', 'Precision']
    metric_scores = [results[metric] for metric in metrics]
    
    # Plot accuracy and precision scores
    bars = plt.bar(metrics, metric_scores, color=['skyblue', 'lightgreen'])
    plt.title('Model Evaluation Metrics')
    plt.ylabel('Score')
    plt.ylim(0, 1)
    plt.grid(True)
    
    # Add actual values as annotations
    for bar, score in zip(bars, metric_scores):
        plt.text(bar.get_x() + bar.get_width() / 2, bar.get_height() - 0.1, round(score, 2),
                 ha='center', color='black', fontsize=12, fontweight='bold')
    
    plt.savefig(save_path)
    plt.close()

if __name__ == "__main__":
    # File path for the dataset
    data_path = '../data/raw/health_data.csv'
    
    # Output directory for saving model evaluation plots
    model_report_dir = '../assets/model_report'
    if not os.path.exists(model_report_dir):
        os.makedirs(model_report_dir)
    
    # Load the dataset
    data = load_data(data_path)
    
    # Preprocess the data
    data = preprocess_data(data)
    
    # Split data into features and target variable
    X = data.drop(columns=['Outcome Variable'])
    y = data['Outcome Variable']
    
    # Train and optimize Random Forest model using XGBoost with 10-fold cross-validation
    optimized_rf_model = train_optimize_rf_with_xgb(X, y)
    
    # Split data into training and testing sets
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
    
    # Evaluate the optimized model
    evaluation_results = evaluate_model(optimized_rf_model, X_test, y_test)
    
    # Plot and save evaluation metrics
    plot_evaluation_metrics(evaluation_results, os.path.join(model_report_dir, 'optimized_model_evaluation_metrics.png'))
