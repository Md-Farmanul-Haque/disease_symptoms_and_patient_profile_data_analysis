import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import os

def load_data(data_path: str) -> pd.DataFrame:
    """
    Load data from CSV file into a pandas DataFrame.
    
    Args:
    - data_path (str): Path to the CSV file.
    
    Returns:
    - pd.DataFrame: Loaded DataFrame containing the data.
    """
    data = pd.read_csv(data_path)
    return data

def convert_to_numeric(df: pd.DataFrame) -> pd.DataFrame:
    """
    Convert categorical columns in the DataFrame to numeric values.
    
    Args:
    - df (pd.DataFrame): Input DataFrame containing categorical columns.
    
    Returns:
    - pd.DataFrame: DataFrame with categorical columns converted to numeric values.
    """
    bool_map = {'Yes': 1, 'No': 0, 'Positive': 1, 'Negative': 0}
    df.replace(bool_map, inplace=True)
    return df

def generate_visualizations(df: pd.DataFrame) -> None:
    """
    Generate and save visualizations from the provided DataFrame.
    
    Args:
    - df (pd.DataFrame): Input DataFrame containing health data.
    
    Returns:
    - None
    """
    # Ensure assets directory exists
    assets_dir = '../assets/visual_insights'
    if not os.path.exists(assets_dir):
        os.makedirs(assets_dir)
    
    # Fever distribution by disease
    plt.figure(figsize=(12, 8))
    sns.countplot(x='Disease', hue='Fever', data=df)
    plt.title('Fever Distribution by Disease', fontsize=16)
    plt.xlabel('Disease', fontsize=14)
    plt.ylabel('Count', fontsize=14)
    plt.xticks(rotation=45, fontsize=12)
    plt.yticks(fontsize=12)
    plt.legend(title='Fever', fontsize=12)
    plt.tight_layout()
    plt.savefig(f'{assets_dir}/fever_distribution_by_disease.png')
    plt.close()

    # Outcome distribution by disease
    plt.figure(figsize=(12, 8))
    sns.countplot(x='Disease', hue='Outcome Variable', data=df)
    plt.title('Outcome Distribution by Disease', fontsize=16)
    plt.xlabel('Disease', fontsize=14)
    plt.ylabel('Count', fontsize=14)
    plt.xticks(rotation=45, fontsize=12)
    plt.yticks(fontsize=12)
    plt.legend(title='Outcome', fontsize=12)
    plt.tight_layout()
    plt.savefig(f'{assets_dir}/outcome_distribution_by_disease.png')
    plt.close()

    # Blood Pressure distribution
    plt.figure(figsize=(12, 8))
    sns.countplot(x='Blood Pressure', hue='Outcome Variable', data=df)
    plt.title('Blood Pressure Distribution by Outcome', fontsize=16)
    plt.xlabel('Blood Pressure', fontsize=14)
    plt.ylabel('Count', fontsize=14)
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)
    plt.legend(title='Outcome', fontsize=12)
    plt.tight_layout()
    plt.savefig(f'{assets_dir}/blood_pressure_distribution_by_outcome.png')
    plt.close()

    # Cholesterol Level distribution
    plt.figure(figsize=(12, 8))
    sns.countplot(x='Cholesterol Level', hue='Outcome Variable', data=df)
    plt.title('Cholesterol Level Distribution by Outcome', fontsize=16)
    plt.xlabel('Cholesterol Level', fontsize=14)
    plt.ylabel('Count', fontsize=14)
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)
    plt.legend(title='Outcome', fontsize=12)
    plt.tight_layout()
    plt.savefig(f'{assets_dir}/cholesterol_level_distribution_by_outcome.png')
    plt.close()

    # Histograms
    plt.figure(figsize=(12, 8))
    df['Age'].hist(bins=10)
    plt.title('Age Distribution Histogram', fontsize=16)
    plt.xlabel('Age', fontsize=14)
    plt.ylabel('Frequency', fontsize=14)
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)
    plt.tight_layout()
    plt.savefig(f'{assets_dir}/age_histogram.png')
    plt.close()

    plt.figure(figsize=(12, 8))
    df['Blood Pressure'].hist(bins=10)
    plt.title('Blood Pressure Distribution Histogram', fontsize=16)
    plt.xlabel('Blood Pressure', fontsize=14)
    plt.ylabel('Frequency', fontsize=14)
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)
    plt.tight_layout()
    plt.savefig(f'{assets_dir}/blood_pressure_histogram.png')
    plt.close()

    plt.figure(figsize=(12, 8))
    df['Cholesterol Level'].hist(bins=10)
    plt.title('Cholesterol Level Distribution Histogram', fontsize=16)
    plt.xlabel('Cholesterol Level', fontsize=14)
    plt.ylabel('Frequency', fontsize=14)
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)
    plt.tight_layout()
    plt.savefig(f'{assets_dir}/cholesterol_level_histogram.png')
    plt.close()

    # Line graphs
    plt.figure(figsize=(12, 8))
    df.groupby('Age').size().plot(kind='line')
    plt.title('Disease Frequency by Age', fontsize=16)
    plt.xlabel('Age', fontsize=14)
    plt.ylabel('Frequency', fontsize=14)
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)
    plt.tight_layout()
    plt.savefig(f'{assets_dir}/disease_frequency_by_age.png')
    plt.close()

    plt.figure(figsize=(12, 8))
    df.groupby('Age')['Outcome Variable'].mean().plot(kind='line')
    plt.title('Average Outcome by Age', fontsize=16)
    plt.xlabel('Age', fontsize=14)
    plt.ylabel('Average Outcome', fontsize=14)
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)
    plt.tight_layout()
    plt.savefig(f'{assets_dir}/average_outcome_by_age.png')
    plt.close()

    # Box plot for age distribution by outcome
    plt.figure(figsize=(12, 8))
    sns.boxplot(x='Outcome Variable', y='Age', data=df)
    plt.title('Age Distribution by Outcome', fontsize=16)
    plt.xlabel('Outcome', fontsize=14)
    plt.ylabel('Age', fontsize=14)
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)
    plt.tight_layout()
    plt.savefig(f'{assets_dir}/age_distribution_by_outcome.png')
    plt.close()

if __name__ == "__main__":
    # File path for the dataset
    data_path = '../data/raw/health_data.csv'
    
    # Load the dataset
    data = load_data(data_path)
    
    # Convert categorical columns to numeric
    data = convert_to_numeric(data)
    
    # Generate and save visualizations
    generate_visualizations(data)
