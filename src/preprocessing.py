import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import os

def load_data(data_path: str) -> pd.DataFrame:
    """
    Load data from CSV file into a pandas DataFrame.
    
    Args:
    - data_path (str): Path to the CSV file.
    
    Returns:
    - pd.DataFrame: Loaded DataFrame containing the data.
    """
    data = pd.read_csv(data_path)
    return data

def preprocess_data(df: pd.DataFrame) -> pd.DataFrame:
    """
    Perform data preprocessing tasks on the DataFrame.
    
    Args:
    - df (pd.DataFrame): Input DataFrame containing health data.
    
    Returns:
    - pd.DataFrame: Preprocessed DataFrame.
    """
    # Convert categorical columns to numeric
    bool_map = {'Yes': 1, 'No': 0, 'Positive': 1, 'Negative': 0}
    df.replace(bool_map, inplace=True)
    
    return df

def generate_age_distribution_plot(df: pd.DataFrame, save_path: str) -> None:
    """
    Generate and save the Age Distribution plot as a PNG file.
    
    Args:
    - df (pd.DataFrame): Input DataFrame containing health data.
    - save_path (str): Path to save the generated PNG file.
    
    Returns:
    - None
    """
    plt.figure(figsize=(8, 6))
    sns.histplot(df['Age'], bins=10, kde=True)
    plt.title('Age Distribution', fontsize=16)
    plt.xlabel('Age', fontsize=14)
    plt.ylabel('Frequency', fontsize=14)
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)
    plt.tight_layout()
    plt.savefig(save_path)
    plt.close()

def generate_gender_distribution_plot(df: pd.DataFrame, save_path: str) -> None:
    """
    Generate and save the Gender Distribution plot as a PNG file.
    
    Args:
    - df (pd.DataFrame): Input DataFrame containing health data.
    - save_path (str): Path to save the generated PNG file.
    
    Returns:
    - None
    """
    plt.figure(figsize=(8, 6))
    df['Gender'].value_counts().plot(kind='bar', color='skyblue')
    plt.title('Gender Distribution', fontsize=16)
    plt.xlabel('Gender', fontsize=14)
    plt.ylabel('Count', fontsize=14)
    plt.xticks(rotation=0, fontsize=12)
    plt.yticks(fontsize=12)
    plt.tight_layout()
    plt.savefig(save_path)
    plt.close()

def generate_outcome_distribution_plot(df: pd.DataFrame, save_path: str) -> None:
    """
    Generate and save the Outcome Variable Distribution plot as a PNG file.
    
    Args:
    - df (pd.DataFrame): Input DataFrame containing health data.
    - save_path (str): Path to save the generated PNG file.
    
    Returns:
    - None
    """
    plt.figure(figsize=(8, 6))
    df['Outcome Variable'].value_counts().plot(kind='bar', color='lightgreen')
    plt.title('Outcome Variable Distribution', fontsize=16)
    plt.xlabel('Outcome', fontsize=14)
    plt.ylabel('Count', fontsize=14)
    plt.xticks(rotation=0, fontsize=12)
    plt.yticks(fontsize=12)
    plt.tight_layout()
    plt.savefig(save_path)
    plt.close()

def generate_correlation_heatmap_plot(df: pd.DataFrame, save_path: str) -> None:
    """
    Generate and save the Correlation Heatmap plot as a PNG file.
    
    Args:
    - df (pd.DataFrame): Input DataFrame containing health data.
    - save_path (str): Path to save the generated PNG file.
    
    Returns:
    - None
    """
    # Select only numeric columns for correlation analysis
    numeric_cols = df.select_dtypes(include='number')
    
    plt.figure(figsize=(10, 8))
    corr = numeric_cols.corr()
    sns.heatmap(corr, annot=True, cmap='coolwarm', cbar=True)
    plt.title('Correlation Heatmap', fontsize=16)
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)
    plt.tight_layout()
    plt.savefig(save_path)
    plt.close()

if __name__ == "__main__":
    # File path for the dataset
    data_path = '../data/raw/health_data.csv'
    
    # Output directory for saving plots
    assets_dir = '../assets/data_report/'
    if not os.path.exists(assets_dir):
        os.makedirs(assets_dir)
    
    # Load the dataset
    data = load_data(data_path)
    
    # Perform data preprocessing
    data = preprocess_data(data)
    
    # Generate and save individual plots
    generate_age_distribution_plot(data, os.path.join(assets_dir, 'age_distribution.png'))
    generate_gender_distribution_plot(data, os.path.join(assets_dir, 'gender_distribution.png'))
    generate_outcome_distribution_plot(data, os.path.join(assets_dir, 'outcome_distribution.png'))
    generate_correlation_heatmap_plot(data, os.path.join(assets_dir, 'correlation_heatmap.png'))
