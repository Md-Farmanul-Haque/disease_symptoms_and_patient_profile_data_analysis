# Disease Symptoms and Patient Profile Data Analysis

## Objective

The objective of this project is to classify the data into negative or positive for the disease. Additionally, a Power BI dashboard is created to gain insights into the data.

## Directory Structure
```
disease_symptoms_and_patient_profile_data_analysis/
│
├── assets/
│   ├── data_report/              # Contains images for data reports
│   ├── miscellaneous/            # Contains miscellaneous images
│   ├── model_report/             # Contains images for model reports
│   └── visual_insights/          # Contains visual insights images
│
├── dashboard/
│   ├── main_dashboard.pbix       # Main Power BI dashboard file
│   └── dashboard_images/         # Relevant images of the dashboard
│
├── data/
│   ├── raw/                      # Houses raw data
│   └── processed/                # Houses processed data
│
├── src/
│   ├── preprocessing.py          # Data preprocessing script
│   ├── visualisation.py          # Data visualization script
│   ├── model_training.py         # Model training script
│   └── optimisation_random_forest.py # Random Forest optimization script
│
├── LICENSE                       # License file
├── README.md                     # Project documentation (this file)
└── requirements.txt              # List of required packages
```

## Setup Instructions

### Prerequisites
- Python 3.x
- Power BI Desktop

### Installation

1. Clone the repository:
    ```bash
    git clone https://gitlab.com/Md-Farmanul-Haque/disease_symptoms_and_patient_profile_data_analysis.git
    
    
    cd disease_symptoms_and_patient_profile_data_analysis
    ```

2. Create and activate a virtual environment:
    ```bash
    python -m venv venv
    
    
    source venv/bin/activate   # On Windows use `venv\Scripts\activate`
    ```

3. Install the required packages:
   
    ```bash
    pip install -r requirements.txt
    ```

### Running the Scripts

1. **Data Preprocessing:**
    
    ```bash
    python src/preprocessing.py
    ```

2. **Data Visualization:**
    ```bash
    python src/visualisation.py
    ```

3. **Model Training:**
    ```bash
    python src/model_training.py
    ```

4. **Model Optimization:**
    ```bash
    python src/optimisation_random_forest.py
    ```

### Power BI Dashboard

- Open `dashboard/main_dashboard.pbix` with Power BI Desktop to view and interact with the dashboard.

## Contact Information
- **Name:** Md Farmanul Haque
- **Email:** [farmanhaque74@gmail.com](mailto:farmanhaque74@gmail.com)
- **GitLab URL:** [https://gitlab.com/Md-Farmanul-Haque/disease_symptoms_and_patient_profile_data_analysis](https://gitlab.com/Md-Farmanul-Haque/disease_symptoms_and_patient_profile_data_analysis)

## License
This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for more details.

---

Make sure to customize any sections further if necessary and replace placeholder text with actual content where needed.